package ru.tsc.panteleev.tm.api;

import org.jetbrains.annotations.NotNull;

public interface ILoggerService {

    void writeLog(@NotNull String message);

}
