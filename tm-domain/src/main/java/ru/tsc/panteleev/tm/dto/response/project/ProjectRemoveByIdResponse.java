package ru.tsc.panteleev.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public class ProjectRemoveByIdResponse extends AbstractProjectResponse {

    public ProjectRemoveByIdResponse(@Nullable ProjectDTO project) {
        super(project);
    }

}
