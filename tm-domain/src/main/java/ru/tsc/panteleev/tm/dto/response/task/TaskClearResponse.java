package ru.tsc.panteleev.tm.dto.response.task;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class TaskClearResponse extends AbstractTaskResponse {

}
