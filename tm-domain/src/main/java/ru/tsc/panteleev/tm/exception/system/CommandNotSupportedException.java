package ru.tsc.panteleev.tm.exception.system;

import ru.tsc.panteleev.tm.exception.AbstractException;

public final class CommandNotSupportedException extends AbstractException {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(String command) {
        super("Error! Command '" + command + "' not supported...");
    }
}
